(function() {
  var template = Handlebars.template, templates = OCA.External.Templates = OCA.External.Templates || {};
templates['icon'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<li data-icon=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":1,"column":15},"end":{"line":1,"column":23}}}) : helper)))
    + "\">\n	<div class=\"img\">\n		<img src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"url") || (depth0 != null ? lookupProperty(depth0,"url") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":3,"column":12},"end":{"line":3,"column":19}}}) : helper)))
    + "\">\n	</div>\n	<span class=\"name\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":5,"column":20},"end":{"line":5,"column":28}}}) : helper)))
    + "</span>\n	<span class=\"icon icon-delete\" title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"deleteTXT") || (depth0 != null ? lookupProperty(depth0,"deleteTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"deleteTXT","hash":{},"data":data,"loc":{"start":{"line":6,"column":39},"end":{"line":6,"column":52}}}) : helper)))
    + "\"></span>\n</li>\n";
},"useData":true});
templates['site'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isSelected")||(depth0 && lookupProperty(depth0,"isSelected"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"code") : depth0),(depths[1] != null ? lookupProperty(depths[1],"lang") : depths[1]),{"name":"isSelected","hash":{},"data":data,"loc":{"start":{"line":12,"column":12},"end":{"line":12,"column":37}}}),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":12,"column":6},"end":{"line":16,"column":13}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"code") || (depth0 != null ? lookupProperty(depth0,"code") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"code","hash":{},"data":data,"loc":{"start":{"line":13,"column":22},"end":{"line":13,"column":30}}}) : helper)))
    + "\" selected=\"selected\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":13,"column":52},"end":{"line":13,"column":60}}}) : helper)))
    + "</option>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"code") || (depth0 != null ? lookupProperty(depth0,"code") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"code","hash":{},"data":data,"loc":{"start":{"line":15,"column":22},"end":{"line":15,"column":30}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":15,"column":32},"end":{"line":15,"column":40}}}) : helper)))
    + "</option>\n";
},"6":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isSelected")||(depth0 && lookupProperty(depth0,"isSelected"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"device") : depth0),(depths[1] != null ? lookupProperty(depths[1],"device") : depths[1]),{"name":"isSelected","hash":{},"data":data,"loc":{"start":{"line":34,"column":12},"end":{"line":34,"column":41}}}),{"name":"if","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.program(9, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":34,"column":6},"end":{"line":38,"column":13}}})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"device") || (depth0 != null ? lookupProperty(depth0,"device") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data,"loc":{"start":{"line":35,"column":22},"end":{"line":35,"column":32}}}) : helper)))
    + "\" selected=\"selected\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":35,"column":54},"end":{"line":35,"column":62}}}) : helper)))
    + "</option>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"device") || (depth0 != null ? lookupProperty(depth0,"device") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data,"loc":{"start":{"line":37,"column":22},"end":{"line":37,"column":32}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":37,"column":34},"end":{"line":37,"column":42}}}) : helper)))
    + "</option>\n";
},"11":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isSelected")||(depth0 && lookupProperty(depth0,"isSelected"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"icon") : depth0),(depths[1] != null ? lookupProperty(depths[1],"icon") : depths[1]),{"name":"isSelected","hash":{},"data":data,"loc":{"start":{"line":49,"column":12},"end":{"line":49,"column":37}}}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":49,"column":6},"end":{"line":53,"column":13}}})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"icon") || (depth0 != null ? lookupProperty(depth0,"icon") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data,"loc":{"start":{"line":50,"column":22},"end":{"line":50,"column":30}}}) : helper)))
    + "\" selected=\"selected\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":50,"column":52},"end":{"line":50,"column":60}}}) : helper)))
    + "</option>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"icon") || (depth0 != null ? lookupProperty(depth0,"icon") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data,"loc":{"start":{"line":52,"column":22},"end":{"line":52,"column":30}}}) : helper)))
    + "\"><img class=\"svg action delete-button\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"deleteIMG") || (depth0 != null ? lookupProperty(depth0,"deleteIMG") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"deleteIMG","hash":{},"data":data,"loc":{"start":{"line":52,"column":75},"end":{"line":52,"column":88}}}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"removeSiteTXT") || (depth0 != null ? lookupProperty(depth0,"removeSiteTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"removeSiteTXT","hash":{},"data":data,"loc":{"start":{"line":52,"column":97},"end":{"line":52,"column":114}}}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":52,"column":117},"end":{"line":52,"column":125}}}) : helper)))
    + "</option>\n";
},"16":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isSelected")||(depth0 && lookupProperty(depth0,"isSelected"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"type") : depth0),(depths[1] != null ? lookupProperty(depths[1],"type") : depths[1]),{"name":"isSelected","hash":{},"data":data,"loc":{"start":{"line":64,"column":12},"end":{"line":64,"column":37}}}),{"name":"if","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.program(19, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":64,"column":6},"end":{"line":68,"column":13}}})) != null ? stack1 : "");
},"17":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"type") || (depth0 != null ? lookupProperty(depth0,"type") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data,"loc":{"start":{"line":65,"column":22},"end":{"line":65,"column":30}}}) : helper)))
    + "\" selected=\"selected\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":65,"column":52},"end":{"line":65,"column":60}}}) : helper)))
    + "</option>\n";
},"19":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"type") || (depth0 != null ? lookupProperty(depth0,"type") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data,"loc":{"start":{"line":67,"column":22},"end":{"line":67,"column":30}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":67,"column":32},"end":{"line":67,"column":40}}}) : helper)))
    + "</option>\n";
},"21":function(container,depth0,helpers,partials,data) {
    return " checked=\"checked\"";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<li data-site-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":1,"column":18},"end":{"line":1,"column":24}}}) : helper)))
    + "\">\n	<input type=\"text\" class=\"site-name trigger-save\" name=\"site-name\" value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":2,"column":75},"end":{"line":2,"column":83}}}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"nameTXT") || (depth0 != null ? lookupProperty(depth0,"nameTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"nameTXT","hash":{},"data":data,"loc":{"start":{"line":2,"column":98},"end":{"line":2,"column":109}}}) : helper)))
    + "\">\n	<input type=\"text\" class=\"site-url trigger-save\"  name=\"site-url\" value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"url") || (depth0 != null ? lookupProperty(depth0,"url") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":3,"column":74},"end":{"line":3,"column":81}}}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"urlTXT") || (depth0 != null ? lookupProperty(depth0,"urlTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlTXT","hash":{},"data":data,"loc":{"start":{"line":3,"column":96},"end":{"line":3,"column":106}}}) : helper)))
    + "\">\n	<a class=\"icon-more\" href=\"#\"></a>\n\n	<div class=\"options hidden\">\n		<div>\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"languageTXT") || (depth0 != null ? lookupProperty(depth0,"languageTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"languageTXT","hash":{},"data":data,"loc":{"start":{"line":9,"column":10},"end":{"line":9,"column":25}}}) : helper)))
    + "</span>\n				<select class=\"site-lang trigger-save\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(lookupProperty(helpers,"getLanguages")||(depth0 && lookupProperty(depth0,"getLanguages"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"lang") : depth0),{"name":"getLanguages","hash":{},"data":data,"loc":{"start":{"line":11,"column":13},"end":{"line":11,"column":32}}}),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":5},"end":{"line":17,"column":14}}})) != null ? stack1 : "")
    + "				</select>\n			</label>\n		</div>\n\n		<div>\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"groupsTXT") || (depth0 != null ? lookupProperty(depth0,"groupsTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"groupsTXT","hash":{},"data":data,"loc":{"start":{"line":24,"column":10},"end":{"line":24,"column":23}}}) : helper)))
    + "</span>\n				<input type=\"hidden\" name=\"site-groups\" class=\"site-groups\" value=\""
    + alias4((lookupProperty(helpers,"join")||(depth0 && lookupProperty(depth0,"join"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"groups") : depth0),{"name":"join","hash":{},"data":data,"loc":{"start":{"line":25,"column":71},"end":{"line":25,"column":88}}}))
    + "\" style=\"width: 320px;\" />\n			</label>\n		</div>\n\n		<div>\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"devicesTXT") || (depth0 != null ? lookupProperty(depth0,"devicesTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"devicesTXT","hash":{},"data":data,"loc":{"start":{"line":31,"column":10},"end":{"line":31,"column":24}}}) : helper)))
    + "</span>\n				<select class=\"site-device trigger-save\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(lookupProperty(helpers,"getDevices")||(depth0 && lookupProperty(depth0,"getDevices"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"device") : depth0),{"name":"getDevices","hash":{},"data":data,"loc":{"start":{"line":33,"column":13},"end":{"line":33,"column":32}}}),{"name":"each","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":33,"column":5},"end":{"line":39,"column":14}}})) != null ? stack1 : "")
    + "				</select>\n			</label>\n		</div>\n\n		<div>\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"iconTXT") || (depth0 != null ? lookupProperty(depth0,"iconTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"iconTXT","hash":{},"data":data,"loc":{"start":{"line":46,"column":10},"end":{"line":46,"column":21}}}) : helper)))
    + "</span>\n				<select class=\"site-icon trigger-save\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(lookupProperty(helpers,"getIcons")||(depth0 && lookupProperty(depth0,"getIcons"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"icon") : depth0),{"name":"getIcons","hash":{},"data":data,"loc":{"start":{"line":48,"column":13},"end":{"line":48,"column":28}}}),{"name":"each","hash":{},"fn":container.program(11, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":48,"column":5},"end":{"line":54,"column":14}}})) != null ? stack1 : "")
    + "				</select>\n			</label>\n		</div>\n\n		<div>\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"positionTXT") || (depth0 != null ? lookupProperty(depth0,"positionTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"positionTXT","hash":{},"data":data,"loc":{"start":{"line":61,"column":10},"end":{"line":61,"column":25}}}) : helper)))
    + "</span>\n				<select class=\"site-type trigger-save\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(lookupProperty(helpers,"getTypes")||(depth0 && lookupProperty(depth0,"getTypes"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"type") : depth0),{"name":"getTypes","hash":{},"data":data,"loc":{"start":{"line":63,"column":13},"end":{"line":63,"column":28}}}),{"name":"each","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":63,"column":5},"end":{"line":69,"column":14}}})) != null ? stack1 : "")
    + "				</select>\n			</label>\n		</div>\n\n		<div class=\"site-redirect-box\">\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"redirectTXT") || (depth0 != null ? lookupProperty(depth0,"redirectTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"redirectTXT","hash":{},"data":data,"loc":{"start":{"line":76,"column":10},"end":{"line":76,"column":25}}}) : helper)))
    + "</span>\n				<input type=\"checkbox\" id=\"site_redirect_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":77,"column":45},"end":{"line":77,"column":51}}}) : helper)))
    + "\" name=\"site_redirect_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":77,"column":73},"end":{"line":77,"column":79}}}) : helper)))
    + "\"\n					   value=\"1\" class=\"site-redirect checkbox trigger-save\" "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"redirect") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":78,"column":62},"end":{"line":78,"column":103}}})) != null ? stack1 : "")
    + " />\n				<label for=\"site_redirect_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":79,"column":30},"end":{"line":79,"column":36}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"noEmbedTXT") || (depth0 != null ? lookupProperty(depth0,"noEmbedTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"noEmbedTXT","hash":{},"data":data,"loc":{"start":{"line":79,"column":38},"end":{"line":79,"column":52}}}) : helper)))
    + "</label>\n			</label>\n		</div>\n\n		<div class=\"site-target-box\">\n			<label>\n				<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"targetTXT") || (depth0 != null ? lookupProperty(depth0,"targetTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"targetTXT","hash":{},"data":data,"loc":{"start":{"line":85,"column":10},"end":{"line":85,"column":23}}}) : helper)))
    + "</span>\n				<input type=\"checkbox\" id=\"site_target_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":86,"column":43},"end":{"line":86,"column":49}}}) : helper)))
    + "\" name=\"site_target_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":86,"column":69},"end":{"line":86,"column":75}}}) : helper)))
    + "\"\n					   value=\"1\" class=\"site-target checkbox trigger-save\" "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"target") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":87,"column":60},"end":{"line":87,"column":99}}})) != null ? stack1 : "")
    + " />\n				<label for=\"site_target_"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":88,"column":28},"end":{"line":88,"column":34}}}) : helper)))
    + "\"></label>\n			</label>\n		</div>\n\n		<div class=\"button delete-button\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"removeSiteTXT") || (depth0 != null ? lookupProperty(depth0,"removeSiteTXT") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"removeSiteTXT","hash":{},"data":data,"loc":{"start":{"line":92,"column":36},"end":{"line":92,"column":53}}}) : helper)))
    + "</div>\n	</div>\n</li>\n";
},"useData":true,"useDepths":true});
})();